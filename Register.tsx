import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      mail: '',
      password: '',
    };
  }


  handleRegister = () => {
    const { username, mail, password } = this.state;
    this.setState({});
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  
  var raw = JSON.stringify({
    username: '',
    mail: '',
    password: ''
  });
  
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };
  
  fetch("https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json", requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));

  }


  render() {
    return(
<SafeAreaView
      // settingan background dan content
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center', // penempatan posisi content
        alignItems: 'center',
      }}>
      <TextInput
        // settingan content input name
        placeholder="username"
        keyboardType='default'
        placeholderTextColor={'#bed509'}
        onChangeText={(name) => this.setState({name})}
        value={this.state.email}
        // settingan kotak border
        style={{
          borderColor: 'grey',
          borderWidth: 1, // ketebalan border
          borderRadius: 20, // curve border, bergantung dgn height
          width: 200,
          height: 40,
          alignSelf: 'center',
          textAlign: 'center',
        }}
      />
      <TextInput
        // settingan content input name
        placeholder="mail"
        keyboardType='email-address'
        placeholderTextColor={'#bed509'}
        onChangeText={(mail) => this.setState({mail})}
        value={this.state.mail}
        
        // settingan kotak border
        style={{
          borderColor: 'grey',
          borderWidth: 1, // ketebalan border
          borderRadius: 20, // curve border, bergantung dgn height
          width: 200,
          height: 40,
          top: 10,
          alignSelf: 'center',
          textAlign: 'center',
        }}
      />
<TextInput
        // settingan content input name
        placeholder="Password"
        keyboardType="default"
        placeholderTextColor={'#bed509'}
        secureTextEntry={true}
        onChangeText={(name) => this.setState({name})}
        value={this.state.email}
        
        // settingan kotak border
        style={{
          borderColor: 'grey',
          borderWidth: 1, // ketebalan border
          borderRadius: 20, // curve border, bergantung dgn height
          width: 200,
          height: 40,
          top: 20,
          alignSelf: 'center',
          textAlign: 'center',
        }}
      />


      <View>
        <TouchableOpacity
          // func button enter
          onPress={() => {
            ambil();
          }}>
          <Text
            // settingan tampilan button enter
            style={{
              borderWidth: 2,
              borderColor: 'white',
              borderRadius: 10,
              marginTop: 40, //jarak antar kotak enter dgn input
              padding: 5, //tinggi pendek kotak enter
              width: 70, //panjang pendek kotak enter
              backgroundColor: '#bed509', //background tulisan enter
              color: 'white', //warna tulisan enter
              overflow: 'hidden', //penyesuaian borderRadius ios
              textAlign: 'center',
            }}>
            Enter
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
    )
  }



}

  export default Register;