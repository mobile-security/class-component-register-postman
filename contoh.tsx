import {
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    TextInput,
    Image,
  } from 'react-native';
  import React, {useState} from 'react';
  import iNadi from './icons/nadi.png';
  import iBackbutton from './icons/backbutton.png';
  
  const Register = ({navigation}) => {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const ambil = () => {
      var myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');
  
      var raw = JSON.stringify({
        name: name,
        password: password,
      });
  
      var requestOptions = {
        method: 'PATCH',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
      };
  
      fetch(
        // fetch ini adalah sebuah function yang ada dari javascript untuk ambil data API atau data back-end
        'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users/-NMalhNh5LPYMCq7uuUh.json',
        requestOptions,
      )
        // blob
        // maksud dari then adalah ketika api sudah di jalan maka ambil data nya
        .then(response =>
          // kita convert ke text dulu
          response.text(),
        )
        .then(result => {
          // isi yang sudah di convert dari si text
          console.log(result);
          setName(result);
          setPassword(result);
        })
        // catch adalah unutk menangkap data yang error
  
        .catch(error => console.log('error', error));
    };
    // untuk memunculkan pop-up saat login (hanya bisa di android)
  
    return (
      <SafeAreaView
        // settingan background dan content
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'center', // penempatan posisi content
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <View>
            <Image
              source={iBackbutton}
              style={{
                resizeMode: 'contain',
                width: 50,
                height: 50,
                marginRight: 250,
              }}></Image>
          </View>
          <Image
            // settingan gambar
            style={{
              width: 300,
              height: 150,
              margin: 20,
            }}
            source={iNadi}
          />
        </TouchableOpacity>
        <TextInput
          // settingan content input name
          placeholder="Email"
          keyboardType="email-address"
          placeholderTextColor={'#bed509'}
          onChangeText={name => {
            setName(name);
          }}
          // settingan kotak border
          style={{
            borderColor: 'grey',
            borderWidth: 1, // ketebalan border
            borderRadius: 20, // curve border, bergantung dgn height
            width: 200,
            height: 40,
            alignSelf: 'center',
            textAlign: 'center',
          }}
        />
        <TextInput
          // settingan content input name
          placeholder="Password"
          keyboardType="default"
          placeholderTextColor={'#bed509'}
          secureTextEntry={true}
          onChangeText={password => {
            setPassword(password);
          }}
          // settingan kotak border
          style={{
            borderColor: 'grey',
            borderWidth: 1, // ketebalan border
            borderRadius: 20, // curve border, bergantung dgn height
            width: 200,
            height: 40,
            top: 10,
            alignSelf: 'center',
            textAlign: 'center',
          }}
        />
  
        <View>
          <TouchableOpacity
            // func button enter
            onPress={() => {
              ambil();
            }}>
            <Text
              // settingan tampilan button enter
              style={{
                borderWidth: 2,
                borderColor: 'white',
                borderRadius: 10,
                marginTop: 20, //jarak antar kotak enter dgn input
                padding: 5, //tinggi pendek kotak enter
                width: 70, //panjang pendek kotak enter
                backgroundColor: '#bed509', //background tulisan enter
                color: 'white', //warna tulisan enter
                overflow: 'hidden', //penyesuaian borderRadius ios
                textAlign: 'center',
              }}>
              Enter
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  };
  
  export default Register;
  